import logo from './logo.svg';
import './App.css';
import { useState,useEffect } from 'react';
import ChessSquare from './component/ChessSquare';

function App() {

  const makeStructure=()=> {
    const defaultChessBoard = [];
    for(let i=0;i<8;i++){
      let row = [];
      for(let j=0;j<8;j++){
        if(i%2==0){
          if(j%2==0){
            row.push('white');
          }else{
            row.push('black');
          }
        }else{
          if(j%2==0){
            row.push('black');
          }else{
            row.push('white');
          }
        }
      }
      defaultChessBoard.push(row);
    }
    return defaultChessBoard;
  }
  let defaultChess = makeStructure()

  const [chess, setChess] = useState()

  const dir= [[-1, -1], [1, 1], [-1, 1], [1, -1]]

  const onClickChangeColor= (i, j)=> {
    
    console.log(i, j, "lol try again")
    // setChess(defaultChess);

    let temp = [];
    for(let k=0;k<8;k++){
      let now=[]
      for(let l=0;l<8;l++){
        now.push(defaultChess[k][l]);
      }
      temp.push(now);
    }
    console.log(temp, "hehe")

    for(let k=0;k<4;k++){

      let u= i, v= j;
      // console.log(dir[k][0], dir[k][1])
      while(u>=0 && v>=0 && u<8 && v<8){
        console.log(u, v, temp[u][v])
        u+= dir[k][0];
        v+= dir[k][1];
        if(u>=0 && v>=0 && u<8 && v<8)
        temp[u][v]= 'red'

      }
    }
    temp[i][j] = 'red';

    setChess(temp)

  }

  useEffect(()=>{
    setChess(defaultChess)
  }, [] )
  

  // console.log(chess, "hahah")
  
  return (
    <div className='container' >
      {
        chess &&  chess.map((row, indexRow) => {
        // console.log(row, "lol")
            return <div style={{display:"flex"}}>
                {
                  row.map((col, indexCol) => {
                    // console.log(col)
                    return <div onClick={()=> onClickChangeColor(indexRow, indexCol)} >
                      <ChessSquare color={col} />
                    </div>
                      
                  })
                }

            </div>  
              
        })
        
      }
    </div>
  )
}

export default App;
